﻿// classe selada ela nao pode ser Herdada.
using System;

namespace Aula40_Classe_Sealed
{
    sealed class Veiculo{ // Não poder ser usado o conceito de hernaça
        
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
