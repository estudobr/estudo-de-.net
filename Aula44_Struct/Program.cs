﻿
using System;

namespace Aula44_Struct
{

    struct Carro{
        public string marca;
        public string modelo;
        public string cor;

        public Carro(string marca, string modelo, string cor){
            this.marca = marca;
            this.modelo = modelo;
            this.cor = cor;
        }
        public void info(){
            Console.WriteLine("Marca ...: {0}",this.marca);
            Console.WriteLine("Modelo ..: {0}",this.modelo);
            Console.WriteLine("Cor .....: {0}",this.cor);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Carro c1 = new Carro("Mercedes", "320A","Vinho");
            c1.info();
            
            // Carro c1;
            // c1.marca = "GOl";
            // c1.modelo = "Golf";
            // c1.cor = "Vermelho";

            // Console.WriteLine("Marca ...: {0}",c1.marca);
            // Console.WriteLine("Modelo ..: {0}",c1.modelo);
            // Console.WriteLine("Cor .....: {0}",c1.cor);
        }
    }
}
