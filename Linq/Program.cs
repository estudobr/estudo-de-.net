﻿
#region FONTE DE DADOS
var listaProdutos = new List<Produto>()
{
    new Produto
    {
        Id = 9, CategoriaId = 2, Nome = "Geladeira", Status = true, Valor = 10,
        Categorias = new List<Categoria>()
        {
            new Categoria()
            {
                Id = 1, Nome = "Categoria 01", Status = true
            },
            new Categoria()
            {
                Id = 2, Nome = "Categoria 02", Status = true
            }
        }
    },
    new Produto
    {
        Id = 7, CategoriaId = 2, Nome = "Feijão", Status = true, Valor = 12,
        Categorias = new List<Categoria>()
        {
            new Categoria()
            {
                Id = 3, Nome = "Categoria 03", Status = true
            },
            new Categoria()
            {
                Id = 4, Nome = "Categoria 04", Status = true
            }
        }
    },
};

#region Segunda FONTE DE DADOS

var listaProdutos2 = new List<Produto>()
{
    new Produto {Id = 9, CategoriaId = 2, Nome = "Geladeira", Status = true, Valor = 10},
    new Produto {Id = 2, CategoriaId = 3, Nome = "Short", Status = true, Valor = 1},
    new Produto {Id = 4, CategoriaId = 1, Nome = "Maquina de lavar", Status = false, Valor = 32},
    new Produto {Id = 3, CategoriaId = 1, Nome = "Video Game", Status = true, Valor = 99},
    new Produto {Id = 6, CategoriaId = 2, Nome = "Arroz", Status = true, Valor = 55},
    new Produto {Id = 8, CategoriaId = 1, Nome = "TV", Status = true, Valor = 45},
    new Produto {Id = 1, CategoriaId = 3, Nome = "Camiseta", Status = true, Valor = 100},
    new Produto {Id = 5, CategoriaId = 1, Nome = "Microondas", Status = true, Valor = 90},
    new Produto {Id = 7, CategoriaId = 2, Nome = "Feijão", Status = true, Valor = 12},
};

#region 2º SITUAÇÃO PROBLEMAS
//valor total da lista
var valorTotal = listaProdutos2.Sum(prod => prod.Valor);

// media do valor dos produtos
var mediaValorProdutos = listaProdutos2.Average(prod => prod.Valor);

//quantos itens tem na lista
var quantidadeItensLista = listaProdutos2.Count();
var quantidadeItensValorMaiorQue90 = listaProdutos2.Count(prod => prod.Valor > 90);

System.Console.WriteLine($"Valor Total: {valorTotal}");
System.Console.WriteLine($"Media: {mediaValorProdutos}");
System.Console.WriteLine($"Total de Itens: {quantidadeItensLista}");
System.Console.WriteLine($"Total de itens maior que 90: {quantidadeItensValorMaiorQue90}");
System.Console.WriteLine($"Valor Total: ");

//criar uma lista com um range de numeros
var range = Enumerable.Range(1, 12);
foreach (var item in range)
{
    System.Console.WriteLine(item);
}

// criar uma lista com varios itens semelhantes
var listaProdutoIguais = Enumerable.Repeat(new Produto() { Id = 1 }, 5);
foreach (var item in listaProdutoIguais)
{
    System.Console.WriteLine(item.Id);
}

#endregion

#region SITUAÇÃO PROBLEMAS
// ignorar os tres primeiros produtos e pegar o restante
// pegar os 3 primeiros produtos e ignorar o restante
// pegar os dados do meio;
#region modelo raiz
// var tresPrimeiros = new List<Produto>();
// var ignorarTresPrimeiros = new List<Produto>();

// for (int i = 0; i < listaProdutos2.Count; i++)
// {
//     if (i < 3)
//     {
//         tresPrimeiros.Add(listaProdutos2[i]);
//     }

//     if (i > 2)
//     {
//         ignorarTresPrimeiros.Add(listaProdutos2[i]);
//     }
// }

#region Modelo mais atual

// var tresPrimeiros = listaProdutos2.Take(3);
// var ignorarTresPrimeiros = listaProdutos2.Skip(3);

// var dadosDoMeio = listaProdutos2.Skip(3).Take(3);
#endregion

// System.Console.WriteLine("------------------- TRES PRIMEIROS -------------------");
// foreach (var prod in tresPrimeiros)
// {
//     System.Console.WriteLine($"ID: {prod.Id}, NOME: {prod.Nome}");
// }
// System.Console.WriteLine("------------------- IGNORADOS TRES PRIMEIROS -------------------");
// foreach (var prod in ignorarTresPrimeiros)
// {
//     System.Console.WriteLine($"ID: {prod.Id}, NOME: {prod.Nome}");
// }
// System.Console.WriteLine("------------------- DADOS DO MEIO -------------------");

// foreach (var prod in dadosDoMeio)
// {
//     System.Console.WriteLine($"ID: {prod.Id}, NOME: {prod.Nome}");
// }
#endregion

#endregion

#endregion
#region Select many
// select many 

#region modo raiz
// var listaCategoria = new List<Categoria>();

// foreach (var prod in listaProdutos)
// {
//     foreach (var cat in prod.Categorias)
//     {
//         listaCategoria.Add(new Categoria()
//         {
//             Id = cat.Id,
//             Nome = cat.Nome,
//             Status = cat.Status
//         });
//     }
// }
#endregion

#region Modo atualizado pela MS
//var listaCategoria = listaProdutos.SelectMany(prod => prod.Categorias);

#endregion

// foreach (var cat in listaCategoria)
// {
//     System.Console.WriteLine($"ID {cat.Id} NOME {cat.Nome} STATUS {cat.Status}");
// }

#region trazendo apenas o nome da categoria
// var listaCategoriaNomes = listaProdutos.SelectMany(prod => new
//  List<string>(prod.Categorias.Select(cat => cat.Nome)));

//  foreach (var nome in listaCategoriaNomes)
//  {
//     System.Console.WriteLine(nome);
//  }

#endregion

#endregion

// var listaCategoria = new List<Categoria>()
// {
//     new Categoria{Id = 1, Status = true, Nome= "Eletronicos"},
//     new Categoria{Id = 2, Status =  true, Nome= "Alimentos"},
//     new Categoria{Id = 3, Status = true, Nome = "Vestuario"}
// };

#endregion

#region Usando o select
#region Primeiro metodo mais dificil

// var response = new List<ProdutoResponse>();

// foreach (var produto in listaProdutos)
// {
//     response.Add(new ProdutoResponse()
//     {
//         Id = produto.Id,
//         Nome = produto.Nome,
//         Valor = produto.Valor
//     });
// }
#endregion
#region Segundo metodo do jeito mais facil
// var response = listaProdutos.Select(prod => new ProdutoResponse()
// {
//     Id = prod.Id,
//     Nome = prod.Nome,
//     Valor = prod.Valor
// });

#endregion

// foreach (var p in response)
// {
//     System.Console.WriteLine($"ID: {p.Id}, Nome: {p.Nome}, Valor: {p.Valor}");
// }

#endregion


#region Sintaxe de metodos - where | order by | order by descending | revert
// // where
// var resultado1 = listaProdutos.Where(prod => prod.Id >= 2 && prod.Id <= 5);
// // where usando o metodo;
// var resultado = listaProdutos.Where(prod => FiltraProdutoPorValor(prod));

// // order
// resultado = resultado.OrderBy(prod => prod.Id);

// // order by descending
// resultado = resultado.OrderByDescending(prod => prod.Id);

// // revert
// resultado = resultado.Reverse();


// foreach (var result in resultado)
// {
//     System.Console.WriteLine($"ID: {result.Id} | Nome: {result.Nome}");
// }

// static bool FiltraProdutoPorValor(Produto produto)
// {
//     return produto.Valor > 50;
// }

#endregion

#region SINTASE DO METODO DO LINQ

//fisrt -> Primeiro
// last -> ultimo

// var resultado = listaCategoria.FirstOrDefault(x => x.Id == 70);
// System.Console.WriteLine($"{resultado.Nome} | ID: {resultado.Id}");

// Single -> verificar itens duplicados.
// var resultado = listaCategoria.SingleOrDefault(x => x.Id == 4);
// System.Console.WriteLine($"Nome: {resultado?.Nome} | ID: {resultado?.Id}");

#endregion

#region Group
// var result = from prod in listaProdutos
//              group prod by prod.CategoriaId into produtosAgrupados
//              select produtosAgrupados;

// foreach (var item in result)
// {
//     System.Console.WriteLine(item.Key);
//     foreach (var prod in item)
//     {
//         System.Console.WriteLine($"Categoria: {prod.CategoriaId} | Produto: {prod.Nome}");
//     }
// }
#endregion

#region join
//buscar produto e informar o nome do produto e nome d categoria do prosuto
// var resultado = from prod in listaProdutos
//                 join cat in listaCategoria
//                 on prod.CategoriaId equals cat.Id
//                 select new { Produto = prod, Categoria = cat };

// foreach (var item in resultado)
// {
//     System.Console.WriteLine($"Categoria: {item.Categoria.Nome} | Produto: {item.Produto.Nome} | ");
// }
#endregion

#region CRIAR CONSULTA LINQ
//var resultado = from produto in listaProdutos select produto;

// 01 -FILTRA PRODUTOS POR NOME;
// var res = from produto01 in listaProdutos
//           where produto01.Nome.ToLower() == "arRoz".ToLower()
//           select produto01;

// 02 -FILTRA PRIMEIRA LETRA POR NOME;
// var res = from produto01 in listaProdutos
//           where produto01.Nome.ToLower().Substring(0, 1) == "m".ToLower()
//           select produto01;

// 03 -FILTRA PRIMEIRA LETRA POR NOME E STATUS ATIVO
// var resultado = from produto in listaProdutos
//                 where produto.Nome.ToLower().Substring(0, 1) == "m".ToLower() &&
//                 produto.Status == true
// 

//04 - ORDENAR OS PRODUTOS POR ID
// var resultado = from produto in listaProdutos
//                 orderby produto.Id
//                 select produto;

// EXCUTAR A CONSULTA
// foreach (var result in resultado)
// {
//     System.Console.WriteLine($"{result.Id} | {result.Nome} | {result.Valor} | {result.CategoriaId}");
// }
#endregion

#region Outras consultas
// 1 . consulta por nome
// var nomesDoProduto = from prod in listaProdutos select prod.Nome;

// 2. 
// var resultado = from prod in listaProdutos
//                 select new ProdutoDto
//                 { Nome = prod.Nome, Valor = prod.Valor, Status = prod.Status };

// foreach (var result in resultado)
// {
//     System.Console.WriteLine($"{result.Nome} | {result.Valor} | {result.Status} |");
// }
#endregion

class ProdutoResponse
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public decimal Valor { get; set; }
}

class Produto
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public bool Status { get; set; }
    public int Valor { get; set; }
    public int CategoriaId { get; set; }

    public List<Categoria> Categorias { get; set; }
}

class Categoria
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public bool Status { get; set; }
}

class ProdutoDto
{
    public string Nome { get; set; }
    public decimal Valor { get; set; }
    public bool Status { get; set; }
}