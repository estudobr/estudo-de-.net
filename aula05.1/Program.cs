﻿//Operadores
using System;

namespace aula05._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sistema de vendas");

            double valorCompra = 5.50;
            double valorVenda = 0;
            double lucro = 0.1;
            string produto = "Pastel";

            valorVenda = valorCompra + (valorCompra * lucro);

            Console.WriteLine("Produto........:{0,15}", produto);// o 15 é o tamando do espaçamento.
            Console.WriteLine("Val.Compra.....:{0,15:c}", valorCompra);// o <c> é o cifrão.
            Console.WriteLine("Lucro..........:{0,15:p}", lucro);// o <p> é a %
            Console.WriteLine("Val.Venda......:{0,15:c}", valorVenda);// o <p> é a %
        }
    }
}
