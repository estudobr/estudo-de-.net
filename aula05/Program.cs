﻿using System;

namespace aula05
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1, n2, n3;
            n1 = 10; n2 = 10; n3 = 30;           

            bool rest = 10 > 5;
            bool tes = (5 > 3) | (10 < 5);

            int num = 10;
            //num++; //num+=1; //num = num +1 ;
            //num += 10; // num=num*10; 

            // & = AND / E
            // || = or / OU

            Console.WriteLine(rest);
            Console.WriteLine(num);
            Console.WriteLine(tes);
            Console.Write("n1=\t{0}, n2={1}\n n3=\t{2}",n1,n2,n3, "\t\n"); //\t tabulação \n new line
        }
    }
}
