﻿// Constantes
using System;

namespace aula07
{
    class Program
    {
        static void Main(string[] args)
        {
           const string canal= "Cursos";
           const double pi=3.1415;

           Console.WriteLine("canal {0}\nPI: {1}", canal,pi);
        }
    }
}
