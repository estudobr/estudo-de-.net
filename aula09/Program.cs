﻿// Operação de Bitwise

using System;

namespace aula09
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 10;
            int number02 = 10;
            int number03 = 20;

            number = number << 1; // dobra: aumenta a casa da esquuerda
            number02 = number02 >> 1; // metade; reduz a casa da direita
            number03 = number03 << 2;
            // number03 = number03 >> 2;

            Console.WriteLine(number);
            Console.WriteLine(number02);
            Console.WriteLine(number03);
        }
    }
}
