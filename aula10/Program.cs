﻿// Enumeradores;

using System;


namespace aula10
{
    class Program
    {
        enum DiasSemana { Domingo, Segunda, terça, Quarta, Quinta, Sexta, Sábado };
        static void Main(string[] args)
        {
            // DiasSemana ds = DiasSemana.Domingo;
            DiasSemana ds = (DiasSemana)3;
            Console.WriteLine("hoje é: " + ds);

            int dss = (int)DiasSemana.Sexta;

            Console.WriteLine("usando o dds: " + dss);
        }
    }
}
