﻿//conversao de tipo(typecast)
using System;

namespace aula11
{
    class Program
    {
        static void Main(string[] args)
        {
            int n1 = 10;
            float n2 = n1;
            Console.WriteLine(n2);

            float n3 = 10.5f;
            int n4 = (int)n3;

            Console.WriteLine("inteiro com cast: " + n4);

            int vInt = 10;
            short vShort = (short)vInt;
            Console.WriteLine("usando vShort com cast: " + vShort);
        }
    }
}
