﻿// Condição IF-Else
using System;

namespace aula13
{
    class Program
    {
        static void Main(string[] args)
        {
            String resultado;
            int nota01, nota02, nota03, nota04, res;
            res = nota01 = nota02 = nota03 = nota04 = 0;

            Console.Write("Digite a nota 01: ");
            nota01 = int.Parse(Console.ReadLine());

            Console.Write("Digite a nota 02: ");
            nota02 = int.Parse(Console.ReadLine());

            Console.Write("Digite a nota 03: ");
            nota03 = int.Parse(Console.ReadLine());

            Console.Write("Digite a nota 04: ");
            nota04 = int.Parse(Console.ReadLine());

            res = nota01 + nota02 + nota03 + nota04;
            // >= 60 - Aprovado
            //  59 e 40 - Recuperação
            // < 40 - Reprovado

            if (res < 40)
            {
                resultado = "Reprovado";
            }
            else if (res < 60)
            {
                resultado = "Recuperação";
            }
            else
            {
                resultado = "Aprovado";
            }

            Console.WriteLine("Nota: {0} - Resultado: {1}", res, resultado);
        }
    }
}
