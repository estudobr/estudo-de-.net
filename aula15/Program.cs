﻿// usando o Switch case
using System;

namespace aula15
{
    class Program
    {
        static void Main(string[] args)
        {
            int tempo = 0;
            char escolha;

            Console.WriteLine("Belo Horizonte/MG a vitoria/ES");
            Console.WriteLine("Escolhar o tansporte: [a]Avião | [c]Carro | [o]Onibus");
            // sempre fazer a conversao typecast
            escolha = char.Parse(Console.ReadLine());

            switch (escolha)
            {
                case 'a':
                case 'A':
                    tempo = 50;
                    break;
                case 'c':
                case 'C':
                    tempo = 480;
                    break;
                case 'o':
                case 'O':
                    tempo = 660;
                    break;
                default:
                    tempo = -1;
                    break;
            }

            if (tempo<0){
                Console.WriteLine("Transporte indisponível");
            }else{
                Console.WriteLine("Para o transporte escolhido o tempo é : {0} minutos" , tempo);
            }
        }
    }
}
