﻿// Matrizes | vetores Bidimensionais
using System;

namespace aula18
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] n = new int[3, 5];
            int[,] num = new int[2,2]{{10,20},{44,09}};
            /*
            10 20 30 40 50
            32 11 10 90 32
            54 76 89 02 12
            */

            n[0, 0] = 10; n[0, 1] = 20; n[0, 2] = 30; n[0, 3] = 40; n[0, 4] = 50;
            n[1, 0] = 32; n[1, 1] = 11; n[1, 2] = 10; n[1, 3] = 90; n[1, 4] = 42;
            n[2, 0] = 54; n[2, 1] = 76; n[2, 2] = 89; n[2, 3] = 02; n[2, 4] = 12;

            Console.Write(n[1, 3]+" ");
            Console.WriteLine(n[2, 1]);

            Console.WriteLine(num[1, 1]);

            
        }
    }
}
