﻿// Loop While
using System;

namespace aula20
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num = new int[10];
             int [] num2  = new int [10];

            int i = 0;
            int j = num.Length-1;

            while (i < num.Length)
            {
                num[i] = i;
                Console.WriteLine("Crescente {0}", num[i]);
                i++;
            }
            
            Console.WriteLine("\n Fim do Loop\n");

            while (j < num2.Length)
            {
                num2[j] = j;
                Console.WriteLine("Descrecente {0}", num2[j]);
                j--;
            }
        }
    }
}
