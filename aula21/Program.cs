﻿// Lopp DO WHILE
using System;

namespace aula21
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 5;
            string senha = "123";
            string senhauser;
            int tentativas = 0;

            Console.WriteLine("Digite a senha: ");
            senhauser = Console.ReadLine();

            do
            {
                Console.Clear();
                Console.WriteLine("Digite a senha: ");
                senhauser = Console.ReadLine();
                tentativas++;
            } while (senhauser != senha);
            Console.WriteLine("Senha correta\nNúmeros de tentativas: {0}", tentativas);


            while (number < 5)
            {
                Console.WriteLine("Number is maior");
            }

            do
            {
                Console.WriteLine("Sua idade é ? ");
            } while (number < 5);
            {

            }
        }
    }
}
