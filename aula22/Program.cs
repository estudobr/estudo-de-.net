﻿
// Loop FOREACH
using System;

namespace aula22
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] number = new int[3] { 11, 22, 33 };

            for (int i = 0; i < number.Length; i++)
            {
                Console.Write(number[i]);
            }
            Console.WriteLine("\n");

            foreach (int n in number)
            {
                Console.WriteLine(n);
            }
        }
    }
}
