﻿// Métodos para arrays

using System;

namespace aula23
{
    class Program
    {
        static void Main(string[] args)
        {
            int [] vetor1 = new int [5];
            int [] vetor2 = new int [5];
            int [] vetor3 = new int [5];
            int [,] matriz = new int[2,5]{{11,22,00,44,55},{66,77,88,99,00}};

            Random random = new Random();

            for (int i = 0; i < vetor1.Length; i++){
                vetor1[i] = random.Next(50);
            }

            Console.WriteLine("Elementos do vetor1");
            foreach (int n in vetor1){
                Console.WriteLine(n);
            }

            // public static int BinarySearch(Array, valor);
            Console.WriteLine("BinarySearch");
            int procurado = 33;
            int pos = Array.BinarySearch(vetor1,procurado);
            Console.WriteLine("Valor {0} está na posição {1}", procurado,pos);
            Console.WriteLine("___________________________________");

            // public static void Copy(Ar_origen,Ar_destino,qtde_elementos);
            Console.WriteLine("Copy");
            Array.Copy(vetor1,vetor2,vetor1.Length);
            foreach(int n in vetor2){
                Console.WriteLine(n);
            }
            Console.WriteLine("-----------------usando copy------------------------");

            // public long GetLongLength(dimensão);
            Console.WriteLine("GetLongLength");
            long qtdeElementosVetor = vetor1.GetLongLength(0);
            Console.WriteLine("Quantidade de elementos {0} ",qtdeElementosVetor);
            Console.WriteLine("-----------------------------------------");

            // public int GetLowerBound(Dimensão);
            Console.WriteLine("GetLowerBound");
            int MenorIndiceVetor = vetor1.GetLowerBound(0);
            int MenorIndiceMatriz_D1 = matriz.GetLowerBound(1);
            Console.WriteLine("Menor ìndice do veltor1 {0}", MenorIndiceVetor);
            Console.WriteLine("-----------------------------------------");

            //public int GetUpperBound(Dimensão);
            Console.WriteLine("GetUppderBound");
            int MaiorIndiceVetor = vetor1.GetUpperBound(0);
            int MaiorIndiceMatriz = matriz.GetUpperBound(1);
            Console.WriteLine("Maaior índice de vetor1 {0}", MaiorIndiceVetor);
            Console.WriteLine("-----------------------------------------");

            //public object GetValue(Long indice);
            Console.WriteLine("GetValue");
            int valor0 = Convert.ToInt32(vetor1.GetValue(3));
            int valor1 = Convert.ToInt32(matriz.GetValue(1,3));
            Console.WriteLine("Valor da posiao 3 do vertor1: {0}", valor0);
            Console.WriteLine("-----------------------------------------");

            // public static int IndexOf(Array,valor);
            Console.WriteLine("IndexOf");
            int indice1 = Array.IndexOf(vetor1,3);
            Console.WriteLine("Indice do primeiro valor 3:{0}", indice1);
            Console.WriteLine("-----------------------------------------");

            // public static void Reverse(array);
            Array.Reverse(vetor1);
            foreach (int n in vetor1){
                Console.WriteLine(n);
            }

            // public void SetValue(Object valor, long pos);
            vetor2.SetValue(99,0);
            for (int i = 0; i < vetor2.Length; i++){
                vetor2.SetValue(0,i);
            }
            Console.WriteLine("Vetor 2");
            foreach (int n in vetor2){
                Console.WriteLine(n);
            }

            // public static void Sort(array);
            Array.Sort(vetor1);
            Array.Sort(vetor2);
            Array.Sort(vetor3);

            Console.WriteLine("Usando Sort\nVetor1");
            foreach (int n in vetor1){
                Console.WriteLine(n);
            }
            
            Console.WriteLine("\nVetor2");
            foreach (int n2 in vetor2){
                Console.WriteLine(n2);
            }

            Console.WriteLine("\nvetor3");
            foreach (int n3 in vetor3){
                Console.WriteLine(n3);
            }

        }
    }
}

