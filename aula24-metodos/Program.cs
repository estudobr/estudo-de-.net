﻿using System;

namespace aula24_metodos
{
    class Program
    {
        static void Main(string[] args)
        {
            int v1 = 0;
            int v2 = 0;
            int r = 0;

            v1 = Convert.ToInt32(Console.ReadLine());
            v2 = Convert.ToInt32(Console.ReadLine());
            r = soma2(v1, v2);
            Console.WriteLine("A soma de  {0} +  {1}  é = {2}",v1,v2,r);

            ola();
            soma(v1, v2);
            soma(10, 5);

        }

        static void ola(){
            Console.WriteLine("Bom dia");
        }

        static void soma(int n1, int n2){
            int rest = n1 + n2;
            Console.WriteLine("A soma de  {0} +  {1}  é = {2}",n1,n2,rest);
        }

        static int soma2(int n1, int n2){
            int rest = n1 + n2;
            return rest;
        }

    }
}
