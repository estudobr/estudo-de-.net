﻿using System;

namespace aula25_Passagem_valor_e_Referencia
{
    class Program
    {
        static void Main(string[] args)
        {
            int valor1 = 10;
            int valor2 = 2;

            dobrar(valor1); 
            dobrar1(ref valor2); // ref
            Console.WriteLine("Passagem por valor: {0}\nPor referencia: {1}",valor1, valor2);
        }

        static void dobrar(int valor){ // Passagem por valor
            valor *=2;
        }

        static void dobrar1(ref int valor){ // passagem por referencia
            valor *=2;
        }
    }
}
