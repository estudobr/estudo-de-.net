﻿
// Argumento Out
using System;

namespace aula26_Argumento_out
{
    class Program
    {
        static void Main(string[] args)
        {
            int divid, divis, quoc, rest, mais;
            divid = 10;
            divis = 3;
            mais = 1;
            quoc = divide(divid, divis,out rest, out mais);
            

            Console.WriteLine("{0} / {1}: quociente = {2} e resto = {3}",divid,divis,quoc,rest);
            Console.WriteLine("Mais = {0}", mais);
        }

        static int divide(int dividendo, int divisor,out int resto, out int mais){ // out int resto
            int quociente;
            quociente = dividendo/ divisor;
            resto = dividendo % divisor; //tambem pra retorna o resto
            mais = resto + 1;
            return quociente;
            
        }
    }
}
