﻿// Argumento params
using System;

namespace aula27_Argumento_params
{
    class Program
    {
        static void Main(string[] args)
        {
            soma1(10, 5);
        }

        // static vois soma(int nota1, int nota2){
        //     int resultado = nota1 + nota2;

        //     Console.WriteLine("A soma de {0} + {1} = {2}", nota1, nota2, resultado);
        // }

        static void soma1(params int []n){
            int resultado = 0;

            if (n.Length < 1){
                Console.WriteLine("Não existem valores a serem somandos");
            }else if (n.Length < 2){
                Console.WriteLine("valores insuficentes para soma: {0}", n[0]);
            }else
            {
                for (int i = 0; i < n.Length; i++){
                    resultado += n[i];
                }
            }
            Console.WriteLine("A soma dos valores é: {0}", resultado);
        }
    }
}
