﻿// Classes e objetos
using System;

namespace aula28_Classes_e_Objetos
{
    public class Jogador{
        public int energia = 100;
        public bool vivo = true;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Jogador j1 = new Jogador(); 
            Jogador edivan = new Jogador();

            j1.energia = 50;
            edivan.energia = 99;
            Console.WriteLine("Energia do jogador 1: {0} %", j1.energia);
            Console.WriteLine("Energia do jogador Edivan: {0} %", edivan.energia);
        }
    }
}
