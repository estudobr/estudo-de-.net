﻿using System;

namespace aula30_Sobrecarga_de_Construtores
{
    public class Jogador{
        public int energia;
        public bool vivo;
        public string nome;

        
        public Jogador(){
            energia = 100;
            vivo = true;
            nome = "Jogador";
        }
        public Jogador(string n){
            energia = 100;
            vivo = true;
            nome = n;
        }

        public Jogador(string n, int e){
            energia = 100;
            vivo = true;
            nome = n;
        }
        public Jogador(string n,int e, bool v){
            energia = 100;
            vivo = true;
            nome = n;
        }

        public void info (){
            Console.WriteLine("Nome jogador ....: {0}", nome);
            Console.WriteLine("Energia jogador .: {0}", energia);
            Console.WriteLine("Estado jogador ..: {0}\n", vivo);
        }      
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("----------------------------------");
            
            Jogador j1 = new Jogador(); 
            Jogador j2 = new Jogador("Edivan");
            Jogador j3 = new Jogador("Bruno", 100);
            Jogador j4 = new Jogador("zico", 30, true);
            
            j1.info();
            j2.info();
            j3.info();
            j4.info();
        }
    }
}
