﻿// Cadeia de Herença e Construtor da classe base, mais o operador ternario.
using System;

namespace aula35_Cadeia_de_Heranca
{
    class Veiculo{ // class basic
        private int rodas;
        public int velMax;
        private bool ligado;
        public void ligar(){
            ligado = true;
        }

        public Veiculo(int rodas){
            this.rodas = rodas;
        }

        public void desligar() {
            ligado = false;
        }

        public string getLigado(){
            // if (ligado){
            //     return "Sim";
            // }
            // else{
            //     return "Não";
            // }
            // usando o operador ternario
            return (ligado? "Sim":"Não");
        }

        public int getRodas(){
            return rodas;
        }
        public void setRodas(int rodas){
            if (rodas < 0){
                this.rodas = 0;
            }else if (rodas > 4){
                this.rodas = 4;
            }else{
                this.rodas = rodas; 
            }
        }
    }
    class Carro : Veiculo { // classe derivada
        public string nome;
        public string cor;
        public Carro(string nome, string cor):base(2){ // :base(4) acessa o metodo privado de roda
            desligar();            
            velMax = 120;
            this.nome = nome;
            this.cor = cor;
        }
    }

    class CarroCombate:Carro{
        public int monicao;
        public CarroCombate():base("Carro de Combate", "Verde"){
            monicao = 100;
            setRodas(2);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Carro c1 = new Carro("corsa", "vermelho");
            CarroCombate cc1 = new CarroCombate();

            c1.ligar();

            Console.WriteLine("Cor .........: {0}", c1.cor);
            Console.WriteLine("Nome ........: {0}", c1.nome);
            Console.WriteLine("Rodas .......: {0}", c1.getRodas());
            Console.WriteLine("Vel. Maxima .: {0}", c1.velMax);
            Console.WriteLine("Ligado ......: {0}", c1.getLigado());

            Console.WriteLine("----------------");

            Console.WriteLine("Cor .........: {0}", cc1.cor);
            Console.WriteLine("Nome ........: {0}", cc1.nome);
            Console.WriteLine("Rodas .......: {0}", cc1.getRodas());
            Console.WriteLine("Vel. Maxima .: {0}", cc1.velMax);
            Console.WriteLine("Ligado ......: {0}", cc1.getLigado());
            Console.WriteLine("Municao .....: {0}", cc1.monicao);

        }
    }
}
