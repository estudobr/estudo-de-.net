﻿using System;

namespace aula36_Membros_Protected
{
    class Veiculo{ // Base
        public int velAtual;
        private int velMax;
        protected bool ligado;

        public Veiculo(int velMax){
            velAtual = 0;
            this.velMax = velMax;
            ligado = false;
        }

        public bool getLigado(){
            return ligado;
        }
        public int getVelMax(){
            return velMax;
        }
    }

    class Carro:Veiculo{ // Derivada de veiculo
        public string nome;
        public Carro(string nome, int vm):base(vm){ 
            this.nome = nome;
            ligado = true;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Carro carro =  new Carro("Fusca", 120);

            Console.WriteLine("Nome .......: {0}", carro.nome);
            Console.WriteLine("Vel.Máxima .: {0}", carro.getVelMax);
            Console.WriteLine("Ligado   ...: {0}", getLigado);
        }
    }
}
