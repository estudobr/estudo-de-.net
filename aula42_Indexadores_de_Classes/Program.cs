﻿using System;

namespace aula42_Indexadores_de_Classes
{
    class Carro{
        private int[] velMax = new int [5]{80,120,160,230,300};

        public int this[int i]{ // Indexador
            get{
                return velMax[i];
            }
            set{
                if (value < 0){
                    velMax[i] = 0;
                }else if (value > 300){
                    velMax[i] = 300;
                }else{
                    velMax[i] = value;
                }
            }            
        }

        public Carro(){
             
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Carro c1 = new Carro();

            c1[4] = 200;
            Console.WriteLine("Velocidade: {0}", c1[4]);
        }
    }
}
